package com.gp.training.bizlogic.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import javax.annotation.Resource;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;

import com.gp.training.bizlogic.domain.HotelInfo;

@Service
public class HotelInfoDaoImpl implements HotelInfoDao {
	
	private static final String REQUEST_HOTEL_INFO_BY_ID = "SELECT * FROM hotel_info  where id=?";
	
	@Resource(name = "jdbcTemplate")
    private JdbcTemplate jdbcTemplate;
	
	@Override
	public HotelInfo getHotelInfo(Long id) {
		
		final HotelInfo hotelInfo = jdbcTemplate.queryForObject(REQUEST_HOTEL_INFO_BY_ID, new RowMapper<HotelInfo>() {
            @Override
            public HotelInfo mapRow(ResultSet rs, int i) throws SQLException {
            	HotelInfo hotel = new HotelInfo();
            	hotel.setId(rs.getLong("id"));
            	hotel.setName(rs.getString("name"));
            	hotel.setCode(rs.getString("code"));
            	hotel.setDescription("description");           	
                return hotel;
            }
        }, id);
      
		return hotelInfo;		
	}
	
}
