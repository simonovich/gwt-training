package com.gp.training.bizlogic.rest;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.gp.training.bizlogic.dao.HotelInfoDao;
import com.gp.training.bizlogic.domain.HotelInfo;

@Component
@Path("/hotelInfo")
public class HotelInfoResourceImpl {
	
	@Autowired
	HotelInfoDao hotelInfoDao;
	
	@GET
	@Path("{hotelId}")
	@Produces({"application/json"})
	public HotelInfo getHotelInfo(@PathParam("hotelId") long hotelId) {

		HotelInfo hotelInfo = hotelInfoDao.getHotelInfo(hotelId);
		return hotelInfo;
	}
}
